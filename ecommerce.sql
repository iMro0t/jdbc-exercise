create database ecommerce;
use ecommerce;

create table products (
	id int auto_increment,
    name varchar(255),
    mfg_date date,
    exp_date date,
    primary key (id)
);

create table categories (
	id int auto_increment,
    name varchar(255),
    primary key (id)
);

create table product_category (
	id int,
    productId int,
    categoryId int,
    foreign key (productId) references products(id),
    foreign key (categoryId) references categories(id)
);