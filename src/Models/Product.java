package Models;

import java.sql.Date;

public class Product {
    int id, categoryID;
    String name;
    Date mfgDate, expDate;

    public Product(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getExpDate() {
        return expDate;
    }

    public Date getMfgDate() {
        return mfgDate;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }

    public void setMfgDate(Date mfgDate) {
        this.mfgDate = mfgDate;
    }
}
