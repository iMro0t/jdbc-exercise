import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import DB.DBService;
import Dao.CategoryDao;
import Dao.ProductDao;
import Models.Category;
import Models.Product;

public class App {
    public static void main(String[] args) {
        Connection dbConn = null;
        try {
            dbConn = DBService.createConn();
        } catch (SQLException e) {
            System.err.println(e.getSQLState());
        }

        ProductDao pDao = new ProductDao(dbConn);
        CategoryDao cDao = new CategoryDao(dbConn);

        // Print Product Table
        System.out.println("\n\nProduct Table\n\n");
        pDao.getAll().stream().forEach(r -> System.out.println(String.join(" | ", String.valueOf(r.getId()),
                r.getName(), String.valueOf(r.getMfgDate()), String.valueOf(r.getExpDate()))));

        // Print Category Table
        System.out.println("\n\nCategory Table\n\n");
        cDao.getAll().stream()
                .forEach(r -> System.out.println(String.join(" | ", String.valueOf(r.getId()), r.getName())));

        // Print Product Table with Pagination
        int limit = 2;
        int noOfProducts = pDao.count();
        for (int page = 0; page < (noOfProducts % 2 == 0 ? noOfProducts / limit : noOfProducts / limit + 1); page++) {
            System.out.println(String.format("\n\nProduct Page %d\n\n", page));
            pDao.getAll(page * limit, limit).stream()
                    .forEach(r -> System.out.println(String.join(" | ", String.valueOf(r.getId()), r.getName(),
                            String.valueOf(r.getMfgDate()), String.valueOf(r.getExpDate()))));
        }

        // Task 1: Save Single Product
        Product p = new Product("Keyboard");
        p.setExpDate(Date.valueOf("2024-01-01"));
        p.setMfgDate(Date.valueOf("2021-01-01"));
        pDao.save(p);

        // Task 2: Save Single Category
        Category c = new Category("Electronics");
        cDao.save(c);

        // Task 3: Link Product with Cateogry
        Product p3 = pDao.get(1);
        Category c3 = cDao.get(1);
        cDao.linkProduct(c3, p3);

        // Task 6: Update Product
        Product p6 = pDao.get(1);
        if (p6 != null) {
            p6.setName("Keychrone K2");
            p6.setExpDate(Date.valueOf("2022-02-01"));
            pDao.save(p6);
        }

        // Task 7: Update Category
        Category c7 = cDao.get(1);
        if (c7 != null) {
            c7.setName("Awesome Keyboards");
            cDao.save(c7);
        }

        // Task 8: Fetch all products of given category
        Category c8 = cDao.get(2);
        List<Product> products8 = pDao.getAllByCategory(c8);
        System.out.println("\n\nProducts By Category\n\n");
        products8.stream().forEach(r -> System.out.println(r.getId() + " | " + r.getName()));

        // Task 4: Delete Cateogry (by id)
        cDao.delete(cDao.get(1));

        // Task 5: Delete Product (by id)
        pDao.delete(pDao.get(1));
    }

}
