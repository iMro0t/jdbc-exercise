package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Models.Category;
import Models.Product;

public class CategoryDao implements Dao<Category> {
    private Connection conn;

    public CategoryDao(Connection conn) {
        this.conn = conn;
    }

    public void linkProduct(Category c, Product p) {
        if (c == null || p == null)
            return;
        try {
            PreparedStatement pst = conn
                    .prepareStatement("INSERT IGNORE INTO product_category (productId, categoryId) VALUES(?,?)");
            pst.setInt(1, p.getId());
            pst.setInt(2, c.getId());
            pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println(
                    "SQL Error while linking product. Category ID " + c.getId() + ". Error: " + e.getMessage());
        }
    }

    @Override
    public Category get(int id) {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT id, name FROM categories WHERE id=" + id);
            while (rs.next()) {
                Category c = new Category(rs.getString(2));
                c.setId(rs.getInt(1));
                return c;
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting category. Category ID " + id + ". Error: " + e.getMessage());
        }
        return null;
    }

    @Override
    public List<Category> getAll() {
        List<Category> result = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT id, name FROM categories");
            while (rs.next()) {
                Category c = new Category(rs.getString(2));
                c.setId(rs.getInt(1));
                result.add(c);
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting categories list. Error: " + e.getMessage());
        }
        return result;
    }

    @Override
    public void save(Category c) {
        if (c == null)
            return;
        try {
            PreparedStatement pst = conn.prepareStatement("INSERT IGNORE INTO categories (name) VALUES(?)");
            pst.setString(1, c.getName());
            pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println("SQL Error while inserting/updating category, Category ID " + c.getId());
        }
    }

    @Override
    public void delete(Category c) {
        if (c == null)
            return;
        try {
            Statement st = conn.createStatement();
            st.executeUpdate("DELETE FROM product_category where categoryId=" + c.getId());
            st.executeUpdate("DELETE FROM categories where id=" + c.getId());
        } catch (SQLException e) {
            System.out.println(
                    "SQL Error while deleting category, Category ID " + c.getId() + ". Error: " + e.getMessage());
        }
    }
}
