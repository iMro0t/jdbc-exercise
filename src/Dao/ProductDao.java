package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Models.Category;
import Models.Product;

public class ProductDao implements Dao<Product> {
    private Connection conn;

    public ProductDao(Connection conn) {
        this.conn = conn;
    }

    @Override
    public Product get(int id) {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT id, name, mfg_date, exp_date FROM products WHERE id=" + id);
            while (rs.next()) {
                Product p = new Product(rs.getString(2));
                p.setId(rs.getInt(1));
                p.setMfgDate(rs.getDate(3));
                p.setExpDate(rs.getDate(4));
                return p;
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting product. Product ID " + id + ". Error: " + e.getMessage());
        }
        return null;
    }

    public List<Product> getAllByCategory(Category c) {
        List<Product> result = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            String query = String.join(" ", "SELECT p.id, p.name, p.mfg_date, p.exp_date FROM products p",
                    "inner join product_category on", "p.id=product_category.productId inner join",
                    "categories on categories.id=product_category.categoryId");
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                Product p = new Product(rs.getString(2));
                p.setId(rs.getInt(1));
                p.setMfgDate(rs.getDate(3));
                p.setExpDate(rs.getDate(4));
                result.add(p);
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting products list by category. Error: " + e.getMessage());
        }
        return result;
    }

    public int count() {
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT COUNT(id) FROM products");
            if (rs.next())
                return rs.getInt(1);
        } catch (SQLException e) {
            System.out.println("SQL Error while getting count of products. Error: " + e.getMessage());
        }
        return 0;
    }

    public List<Product> getAll(int offset, int limit) {
        List<Product> result = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(String
                    .format("SELECT id, name, mfg_date, exp_date FROM products LIMIT %d OFFSET %d", limit, offset));
            while (rs.next()) {
                Product p = new Product(rs.getString(2));
                p.setId(rs.getInt(1));
                p.setMfgDate(rs.getDate(3));
                p.setExpDate(rs.getDate(4));
                result.add(p);
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting products list. Error: " + e.getMessage());
        }
        return result;
    }

    @Override
    public List<Product> getAll() {
        List<Product> result = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT id, name, mfg_date, exp_date FROM products");
            while (rs.next()) {
                Product p = new Product(rs.getString(2));
                p.setId(rs.getInt(1));
                p.setMfgDate(rs.getDate(3));
                p.setExpDate(rs.getDate(4));
                result.add(p);
            }
        } catch (SQLException e) {
            System.out.println("SQL Error while getting products list. Error: " + e.getMessage());
        }
        return result;
    }

    @Override
    public void save(Product p) {
        if (p == null)
            return;
        try {
            PreparedStatement pst = conn
                    .prepareStatement("INSERT IGNORE INTO products (name, mfg_date, exp_date) VALUES(?,?,?)");
            pst.setString(1, p.getName());
            pst.setDate(2, p.getMfgDate());
            pst.setDate(3, p.getExpDate());
            pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println("SQL Error while inserting/updating product, Product ID " + p.getId());
        }
    }

    @Override
    public void delete(Product p) {
        if (p == null)
            return;
        try {
            Statement st = conn.createStatement();
            st.executeUpdate("DELETE FROM product_category where productId=" + p.getId());
            st.executeUpdate("DELETE FROM products where id=" + p.getId());
        } catch (SQLException e) {
            System.out.println(
                    "SQL Error while deleting product, Product ID " + p.getId() + ". Error: " + e.getMessage());
        }
    }
}
