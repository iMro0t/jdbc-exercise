package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBService {
    private static String url = "jdbc:mysql://localhost:3306/";
    private static String db = "ecommerce";
    private static String userName = "root";
    private static String password = "root";

    public static Connection createConn() throws SQLException {
        return DriverManager.getConnection(url + db, userName, password);
    }

}
